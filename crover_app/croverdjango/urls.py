"""croverdjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from app.views import app_view, graphs_view, site_view, storage_view, no_access_view, user_view, charts_view, webhook_view, test_view, logout_request,  contact_view, registerPage_view, loginPage_view, profile_view
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from app.dash_apps import moistmap, moist, temp, surfaceplot, moisttemp, scatterplot, scatter_unit, mean_time_threshold, heatmap, mean_heatmap, heatmapB, mean_heatmapB, mt_heatmap, historical_trends
import os
from django.conf.urls import include, url
from django.conf import settings

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

STATICFILES_DIRS = os.path.join(BASE_DIR, 'static') # new

urlpatterns = [
    path('register/', registerPage_view, name='register'),
    path('login/', loginPage_view, name='login'),
    path('django_plotly_dash/', include('django_plotly_dash.urls')),
    path('admin/', admin.site.urls),
    path("logout", logout_request, name="logout"),
    path('', app_view, name='app'),
    path('no_access/', no_access_view, name='no_access'),
    path('app/', app_view, name='app'),
    path('graphs/',site_view, name='graphs'), #changes graphs to site
    path('user/',user_view, name='user'),
    path('site/',site_view, name='site'),
    path('storage/',storage_view, name='storage'),
    path('charts/', charts_view, name='charts'),
    path('test/', test_view, name='test'),
    path('webhook/', webhook_view, name='webhook'),
    path('contact/', contact_view, name='contact'),
    path('profile/', profile_view, name='profile'),

]

# urlpatterns += staticfiles_urlpatterns()

# if settings.DEBUG:
#     urlpatterns += [
#     url(r'^__debug__/', include(debug_toolbar.urls))
#     ]

# elif not settings.DEBUG:
#     urlpatterns += staticfiles_urlpatterns()

urlpatterns += staticfiles_urlpatterns()