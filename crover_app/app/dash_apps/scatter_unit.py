import pandas as pd
import plotly.express as px  # (version 4.7.0)
import plotly.graph_objects as go
import numpy as np
import os
import dash  # (version 1.12.0) pip install dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import base64
from django_plotly_dash import DjangoDash
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
app = DjangoDash('scatter_unit')

from ..helper import find_table,get_sites_names
# ------------------------------------------------------------------------------
# App layout
app.layout = html.Div([
    html.H3("Mean Temperature Moisture Scatter Plot"),
    dcc.Dropdown(id="slct_array",
                 options=[
                     {"label": "temperature", "value": "temp"},
                     {"label": "moisture", "value": "moist"}
                      ],
                 multi=False,
                 value="moist",
                 style={'width': '40%', 'display' : 'inline-block',}
                 ),
    dcc.Dropdown(id="coord_array",
                 options=[
                     {"label": "'48.972/169.3274'", "value": "48.972/169.3274"},
                      ],
                 multi=False,
                 value="48.972/169.3274",
                 style={'width': '40%', 'display' : 'inline-block',}
                 ),
    dcc.Dropdown(id="unit_array",                 
                 options=[{'label': j, 'value': j} for j in get_sites_names()],
                 multi=False,
                 value="1",
                 style={'width': '20%', 'display' : 'inline-block',}
                 ),                     
    html.Div(id='output_container', children=[]), 

    dcc.Graph(id='graph'),
]
)

# ------------------------------------------------------------------------------
# Connect the Plotly graphs with Dash Components
# Define callback to update graph
@app.callback(
    [Output('graph', 'figure'),
    Output(component_id='output_container', component_property='children')],
    [Input("slct_array", "value"),Input("coord_array", "value"),Input("unit_array", "value")]
)

def update_figure(slct_array,coord,unit):
    container=("The array chosen by user was: {}, coord: {}, unit: {}.".format(slct_array, coord, unit))
    print(container)
    data=find_table(coord, unit, slct_array)
    print(type(data))
    dim = data['xcoord'].max()

    data=data.drop(['id','timestamp','number','coordinate','imei','xcoord','ycoord'], axis=1)
    data=data.values
    data=pd.DataFrame(data)
    table = pd.DataFrame()
    print("hi")
    print(dim)
    print(data.shape)
    count=0
    for i in data:
        b = np.matrix(data[i].tolist())
        c=b.reshape(int(dim),int(dim))
        df=pd.DataFrame(c)
        a = df.mean(axis=0) #Columns(axis=1)/Rows(axis=0)
        table[count] = a.values.flatten()
        count=count+1
        
#     print(pd.DataFrame(table))

    fig = go.Figure(px.scatter(table ))
    fig.update_layout(
        title="Mean Temperature Scatter Plot",
        yaxis_title="°C")
    
    fig.add_trace(go.Scatter(
        y=table.max(axis=1).values.flatten(),
        x=list(range(0, int(dim+1))),
        name="Max. Temperature"
    ))        
    fig.add_trace(go.Scatter(
        y=table.min(axis=1).values.flatten(),
        x=list(range(0, int(dim+1))),
        name="Min. Temperature"
    ))
    fig.add_trace(go.Scatter(
        y=table.mean(axis=1).values.flatten(),
        x=list(range(0, int(dim+1))),
        name="Mean Temperature"
    ))
    fig.update_layout(
        xaxis_title="Rows",
        legend_title="Arrays/Layers",
#         font=dict(
#             family="Courier New, monospace",
#             size=18,
#             color="RebeccaPurple"
#         )
    )
 
    return fig, container

# ------------------------------------------------------------------------------
