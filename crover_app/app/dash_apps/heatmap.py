# import pandas as pd
# import plotly.express as px  # (version 4.7.0)
# import plotly.graph_objects as go
# import numpy as np
# import os
# import dash  # (version 1.12.0) pip install dash
# import dash_core_components as dcc
# import dash_html_components as html
# from dash.dependencies import Input, Output
# import base64
# from django_plotly_dash import DjangoDash
# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# app = DjangoDash('heatmap')

# from ..helper import find_table,get_sites_names
# # ------------------------------------------------------------------------------

# app.layout = html.Div([
#     html.H3("Temperature Moisture 3D Heat Map"),
#     dcc.Dropdown(id="slct_array",
#                  options=[
#                      {"label": "temperature", "value": "temp"},
#                      {"label": "moisture", "value": "moist"}
#                       ],
#                  multi=False,
#                  value="moist",
#                  style={'width': '40%', 'display' : 'inline-block',}
#                  ),
#     dcc.Dropdown(id="coord_array",
#                  options=[
#                      {"label": "'48.972/169.3274'", "value": "48.972/169.3274"},
#                       ],
#                  multi=False,
#                  value="48.972/169.3274",
#                  style={'width': '40%', 'display' : 'inline-block',}
#                  ),
#     dcc.Dropdown(id="unit_array",
#                  options=[{'label': j, 'value': j} for j in get_sites_names()],
#                  multi=False,
#                  value="1",
#                  style={'width': '20%', 'display' : 'inline-block',}
#                  ),                     
#     html.Div(id='output_container', children=[]), 

#     dcc.Graph(id='graph'),
# ]
# )

# # ------------------------------------------------------------------------------
# # Connect the Plotly graphs with Dash Components
# @app.callback(
#     [Output('graph', 'figure'),
#     Output(component_id='output_container', component_property='children')],
#     [Input("slct_array", "value"),Input("coord_array", "value"),Input("unit_array", "value")]
# )

# def update_figure(slct_array,coord,unit):
#     container=("The array chosen by user was: {}, coord: {}, unit: {}.".format(slct_array, coord, unit))
#     table=find_table(coord, unit, slct_array)
#     if slct_array=="temp":
#         print("temp")
#         color=['#005bb8', '#8ab900','#F05030']
#         unit = "°C"
#     else:
#         color=['#ffffd9','#41b6c4','#081d58']
#         unit = "%"
        
#     dim = table['xcoord'].max()
#     table=table.drop(['id','timestamp','number','coordinate','imei','xcoord', 'ycoord'], axis=1)
#     head=list(range(0, int(dim/2)))
#     table.columns = head    
#     table = table.astype(float)
    
#     #only gets first timestamp
#     table=table.head(dim*dim)

#     z=[(np.matrix(table[i].tolist())).reshape( int(dim), int(dim)) for i in table]
#     data=[]

#     data.append(go.Surface(z=(np.matrix(table.mean(axis=1).tolist())).reshape( int(dim),int(dim)), colorscale=color,showscale=True, opacity=0.9, name="mean"))

#     fig = go.Figure(data=data)

#     for i in range(len(table.columns)):
#         r=z[i]
#         fig.add_trace(go.Surface(z=z[i]+dim*(i+1), text=z[i], hoverinfo='text', meta=i, hovertemplate='x:%{x}m <br>y:%{y}m <br>z: %{meta}m <br><b>value: %{text}</b>'+unit,
#                                  colorscale=color,showscale=False, opacity=0.8, name="array {}".format(table.columns[i])))
    

#     # Add silo image overlay
#     image_filename = os.getcwd() + '/static/images/silo.png' 
#     encoded_image = base64.b64encode(open(image_filename, 'rb').read())
#     fig.add_layout_image(
#         dict(
#             source=('data:image/png;base64,{}'.format(encoded_image.decode())),
#             xref="x",
#             yref="y",
#             x=-4.5,
#             y=7,
#             sizex=13.5,
#             sizey=10,
#             sizing="stretch",
#             opacity=0.3,
#             layer="above")
#     )
    
#     fig.update_yaxes(showgrid=False, scaleanchor='x', showticklabels=False)
#     fig.update_xaxes(showgrid=False, scaleanchor='y',showticklabels=False)
#     fig.update_layout( autosize=False, title= slct_array,title_x=0.5,
#                   height=500,
#                   scene = dict(
#                     xaxis = dict(range=[0,int(dim/2)], title='x: length (m)',showgrid=False,zeroline=False,showline=False,),
#                     yaxis = dict(range=[0,int(dim/2)], title='y: width (m)',showgrid=False),
#                     zaxis = dict(
#                            showticklabels=False, title='z: height (m)',showgrid=False,
#                         )))
 
#     return fig, container

# # ------------------------------------------------------------------------------
