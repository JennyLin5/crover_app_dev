import pandas as pd
import plotly.express as px  # (version 4.7.0)
import plotly.graph_objects as go
import numpy as np
import os
import dash  # (version 1.12.0) pip install dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import base64
from django_plotly_dash import DjangoDash
import dash_bootstrap_components as dbc
from dash.dash import no_update

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

from ..helper import find_table,get_sites_names

app = DjangoDash('historical_trends', meta_tags=[
        {"name": "viewport", "content": "width=device-width, initial-scale=1"}
    ])

# ------------------------------------------------------------------------------

app.layout = html.Div([

    html.Div(
        children = [
            html.Label("Grain store unit",style={'display':'inline-flex','font-size':'15px','padding-right':'20px'}),
 
            dcc.Dropdown(id="unit_array",
                 options=[{'label': j, 'value': j} for j in ['1','2','3','4']],
                 multi=False,
                 placeholder=" ",
                 clearable=False           
                 ),    
        ],
        style={'display':'flex','justify-content':'center', 'align-items':'center'},

    ),

    html.Div([
    dcc.RadioItems(
    options=[
        {'label': 'Moisture', 'value': 'moisture'},
        {'label': 'Temperature', 'value': 'temperature'},
    ],
    labelStyle={"background-color": "lightsteelblue", "padding": "5px 12px 5px 5px", "border-radius":"8px", "margin-right": "10px","height": "15px","align-items":"center","display": "flex"},
    id='moist_or_temp_radiobutton',
    style={"display":"flex"}
),
    dcc.RadioItems(
    options=[
        {'label': 'View by month', 'value': 'month'},
        {'label': 'View by week', 'value': 'week'},
    ],
    labelStyle={"background-color": "lightsteelblue", "padding": "5px 12px 5px 5px", "border-radius":"8px", "margin-right": "10px","height": "15px","align-items":"center","display": "flex"},
    id='month_or_week',
    style={"display":"flex"}
),

    ],
    style={'display':'flex','justify-content':'space-evenly', 'align-items':'center','margin-top':'10px'},
        
    ),
    
    dbc.Row([
        dbc.Col([
            dcc.Graph(id="temp_history_graph", config={
                'displayModeBar': False,'responsive': True,    
            }),
        ], #width={'size':5, 'offset':1},
           xs=12, sm=12, md=12, lg=5, xl=5
        )
    ], align="center")  # Vertical: start, center, end


])

@app.callback(
    Output('temp_history_graph', 'figure'),
    [Input('moist_or_temp_radiobutton', 'value'),
    Input('month_or_week', 'value')]
)
def change_figure_to_moist_or_temp(moist_or_temp, month_or_week):

    if moist_or_temp == None:
        return no_update
    if month_or_week == None:
        return no_update

    if moist_or_temp == 'temperature':
            x=['6/8/2020',	'6/15/2020',	'6/22/2020',	'6/29/2020',	'7/6/2020',	'7/13/2020',	'7/20/2020',	'7/27/2020',	'8/3/2020',	'8/10/2020',	'8/17/2020',	'8/24/2020',	'8/31/2020',	'9/7/2020',	'9/14/2020']
            y_max=[8.6,	9,	8.8,	8.6,	8.6,	8.4,	8.6,	9.2,	10.2,	11,	11.6,	12.8,	15.2,	16.8,	19.4]
            y_avg=[8.2,	8.5,	8.4,	8.2,	8.3,	8.2,	8.2,	7.9,	8.1,	8.6,	8.3,	9,	8,	8.2,	8.4]
            y_min=[4,	4.2,	4.4,	4.2,	4,	3.8,	4,	3.6,	3.8,	4.2,	4,	4.4,	3.8,	4,	4.2]
            graph_type = 'Temperature'
            y_title = 'Temperature (°C)'
            ideal_temp = 12
            y_ideal = np.full(15, ideal_temp)
        
    if moist_or_temp == 'moisture':
        x=['6/8/2020',	'6/15/2020',	'6/22/2020',	'6/29/2020',	'7/6/2020',	'7/13/2020',	'7/20/2020',	'7/27/2020',	'8/3/2020',	'8/10/2020',	'8/17/2020',	'8/24/2020',	'8/31/2020',	'9/7/2020',	'9/14/2020']  
        y_max=[13.2,	13.2,	13.2,	13.2,	13.3,	13.3,	13.4,	13.4,	13.4,	13.4,	15,	15.3,	16,	16.4,	17.6]
        y_avg=[12.8,	12.8,	12.8,	12.8,	12.8,	12.8,	12.9,	12.8,	12.8,	12.8,	12.9,	12.8,	12.9,	12.9,	13]
        y_min=[12.2,	12.3,	12.3,	12.4,	12.2,	12.3,	12.3,	12.3,	12.3,	12.3,	12.2,	12.2,	12.3,	12.4,	12.3]
        graph_type = 'Moisture level'
        y_title = 'Moisture (%)'
        ideal_temp = 14.5
        y_ideal = np.full(15, ideal_temp)

    if month_or_week == 'week':
        fig = go.Figure()

        for i in range(len(x)-3):
            fig.add_trace(
                    go.Scatter(
                        visible=False,
                        line = dict(color='gray',dash='dot'),
                        name="Threshold",
                        mode='lines+markers',
                        x=[x[i],x[i+1],x[i+2],x[i+3]],
                        y=[y_ideal[i],y_ideal[i+1],y_ideal[i+2],y_ideal[i+3]])),
            fig.add_trace(
                    go.Scatter(
                        visible=False,
                        line=dict(color="red"),
                        name="Maximum",
                        mode='lines+markers',
                        x=[x[i],x[i+1],x[i+2],x[i+3]],
                        y=[y_max[i],y_max[i+1],y_max[i+2],y_max[i+3]])),
            fig.add_trace(
                    go.Scatter(
                        visible=False,
                        line=dict(color="orange"),
                        name="Average",
                        mode='lines+markers',
                        x=[x[i],x[i+1],x[i+2],x[i+3]],
                        y=[y_avg[i],y_avg[i+1],y_avg[i+2],y_avg[i+3]])),
            fig.add_trace(
                    go.Scatter(
                        visible=False,
                        line=dict(color="gold"),
                        name="Minimum",
                        mode='lines+markers',
                        x=[x[i],x[i+1],x[i+2],x[i+3]],
                        y=[y_min[i],y_min[i+1],y_min[i+2],y_min[i+3]])),
                    
        fig.data[0].visible = True
        fig.data[1].visible = True
        fig.data[2].visible = True
        fig.data[3].visible = True

        steps = []
        for i in range(round(round(len(fig.data)/4))):
            months = ['June', 'July', 'August']
            if i%4==0:
                label=months[int(i/4)]
            else:
                label=''
            step = dict(
                method="update",
                label=label,
                args=[{"visible": [False] * len(fig.data)}],  
            )
            step["args"][0]["visible"][4*i] = True
            step["args"][0]["visible"][4*i+1] = True
            step["args"][0]["visible"][4*i+2] = True
            step["args"][0]["visible"][4*i+3] = True
            steps.append(step)

        sliders = [dict(
            pad={"t": 50},
            steps=steps
        )]

        fig.update_layout(
            sliders=sliders,
            xaxis_title='Week',
            yaxis_title=y_title
        )
        fig.update_layout(legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="left",
            x=0.01
         ))
        fig.update_yaxes(range=[min(y_min)-1, max(y_max)+1])

        return fig
        
    if month_or_week == 'month':
        fig = go.Figure()

        fig.add_trace(go.Scatter(x=x, y=y_ideal,
                            mode='lines+markers',
                            name='Threshold',line = dict(color='gray',dash='dot')))

        fig.add_trace(go.Scatter(x=x, y=y_max,
                            mode='lines+markers', line=dict(color='red'),
                            name='Maximum'))

        fig.add_trace(go.Scatter(x=x, y=y_avg,
                            mode='lines+markers',
                            name='Average', line = dict(color='orange')))

        fig.add_trace(go.Scatter(x=x, y=y_min,
                            mode='lines+markers',
                            name='Minimum',line = dict(color='gold')))
        fig.update_layout(legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="left",
            x=0.01
         ))
        fig.update_layout( 
            xaxis_title='Month',
            yaxis_title=y_title,
            xaxis = dict(
                tickmode = 'array',
                tickvals = [0, 4, 8, 12],
                ticktext = ['June', 'July', 'August', 'September']
            ),
        )
        fig.update_yaxes(range=[min(y_min)-1, max(y_max)+1])

        return fig


    

