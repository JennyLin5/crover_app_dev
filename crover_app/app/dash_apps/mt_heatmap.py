import plotly.express as px  # (version 4.7.0)
from plotly.subplots import make_subplots
from plotly import tools
import random

import dash  # (version 1.12.0) pip install dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from dash.dash import no_update
import base64
from django_plotly_dash import DjangoDash
import dash_bootstrap_components as dbc

from .mt_heatmap_functions import silo, change_silo_angle, get_updated_moisture_graph, get_updated_temp_graph

external_stylesheets = [dbc.themes.BOOTSTRAP]
app = DjangoDash('mt_heatmap', add_bootstrap_links=True, external_stylesheets=external_stylesheets,
        meta_tags=[
        {"name": "viewport", "content": "width=device-width, initial-scale=1"}
    ])

from ..helper import find_table,get_sites_names
# ------------------------------------------------------------------------------
#Commented out for demo
# x=get_sites_names()
# x.append("20") # for demo purposes 20 = v10 time6
# x.append("21") # for demo 21 = v11 time6

x=['1','2','3','4']  # 1,3 is the old 20, 2,4 is the old 21

app.layout = html.Div([ 

    dcc.Store(id='current-unit-selection-tracker-moisture', storage_type='session'),
    dcc.Store(id='current-unit-selection-tracker-temp', storage_type='session'),

    dbc.Row([
        dbc.Col([
            html.Div(
                children = [
                    html.Label("Grain store unit",style={'display':'inline-flex','font-size':'15px','padding-right':'20px'}),
        
                    dcc.Dropdown(id="unit_array",
                        options=[{'label': j, 'value': j} for j in x],
                        multi=False,
                        placeholder=" ",
                        clearable=False           
                        ),    
                ],
                style={'display':'flex','justify-content':'center', 'align-items':'center'},
            ),    ], width={'size': 8})
    ], justify='center'), 
            
    dbc.Row([
        dbc.Col([    
            html.Div(id='output_container', children=[]),     
            ], width={'size': 50})
    ], justify='center'), 

    # dbc.Row([
    #     dbc.Col([  
    #         html.Label("Moisture",style={'display':'flex','justify-content':'center', 'align-items':'center'},),
    #         ], xs=6, sm=6, md=6, lg=6, xl=6),
    #     dbc.Col([  
    #         html.Label("Temperature",style={'display':'flex','justify-content':'center', 'align-items':'center'},),
    #         ], xs=6, sm=6, md=6, lg=6, xl=6),
            
    # ],), 
    
    html.Div(
        dbc.Row([
            
            dbc.Col([  
                dcc.Loading(type='cube',
                    children=[dcc.Graph(id='moisture_graph', style={'display':'flex','justify-content':'center',},config={'displayModeBar': False,})]
                )        
            ],xs=10, sm=10, md=5, lg=5, xl=5), #xs=0, sm=0, md=0, lg=0, xl=0,
            dbc.Col([
                dcc.Loading(type='dot',
                        children=[dcc.Graph(id='moisture_silo',config={'displayModeBar': False,})]
                        )
                    ],xs=1, sm=1, md=1, lg=1, xl=1), #xs=0, sm=0, md=0, lg=0, xl=0,

            dbc.Col([  
                dcc.Loading(type='cube',
                    children=[dcc.Graph(id='temperature_graph', style={'display':'flex','justify-content':'center',},config={'displayModeBar': False,})]
                )   
            ],xs=10, sm=10, md=5, lg=5, xl=5), #xs=0, sm=0, md=0, lg=0, xl=0,
            dbc.Col([
                dcc.Loading(type='dot',
                        children=[dcc.Graph(id='temperature_silo', config={'displayModeBar': False,})]
                    )
                    ],xs=1, sm=1, md=1, lg=1, xl=1), #xs=0, sm=0, md=0, lg=0, xl=0,
        ], ), 
   ),
    # style={'display':'flex','justify-content':'start', 'align-items':'center',"height":"100%"}
],style={ 'overflow':'hidden'})

# ------------------------------------------------------------------------------
# Connect the Plotly graphs with Dash Components
silo = silo()
initial_rotation = dict(eye=dict(x=2.5, y=2.5, z=1.25))

@app.callback(
    [Output('current-unit-selection-tracker-moisture', 'data'),
    Output('moisture_silo', 'figure')],
    [Input("moisture_graph", "relayoutData"),
    Input("unit_array", "value")],
    [State('current-unit-selection-tracker-moisture', 'data')]
)
def change_silo_angle_when_mositure_changes(data, selectedUnit, currentUnit): 
    return change_silo_angle(data, selectedUnit, currentUnit, silo, initial_rotation)



@app.callback(
    [Output('current-unit-selection-tracker-temp', 'data'),
    Output('temperature_silo', 'figure')],
    [Input("temperature_graph", "relayoutData"),
    Input("unit_array", "value")],
    [State('current-unit-selection-tracker-temp', 'data')]
)
def change_silo_angle_when_mositure_changes(data, selectedUnit, currentUnit): 
    return change_silo_angle(data, selectedUnit, currentUnit, silo, initial_rotation)



@app.callback(
    [Output('moisture_graph', 'figure'),
    Output('temperature_graph', 'figure'),
    Output(component_id='output_container', component_property='children'),
    ],
    [
        Input("unit_array", "value")
    ]
)
def update_graphs(unit):

    if unit != '1' and unit != '2' and unit != '3':
      return no_update

    container=("")
    updated_moisture_fig = get_updated_moisture_graph(unit, initial_rotation)
    updated_temp_graph = get_updated_temp_graph(unit, initial_rotation)
    return updated_moisture_fig, updated_temp_graph, container




# def get_moisture_graph(unit, scene_camera_position):
#     coord='48.972/169.3274'

#     if unit == "2":
#         pathM=os.path.join(BASE_DIR, 'data','moist_v10_6.txt')
#         dim=20 #y=20 z-20
#         dfM = pd.read_csv(pathM, header=None)

#         a = dfM.to_numpy()
#         length = len(a)

#         a_shaped = a.reshape(dim, int(length/dim))
#         a_shaped_transposed = a_shaped.transpose()

#         moist_table=pd.DataFrame(a_shaped_transposed)

#     elif unit == "3":
#         pathM=os.path.join(BASE_DIR, 'data','moist_v11_6.txt')
#         dim=20 #y=20 z-20

#         dfM = pd.read_csv(pathM, header=None)

#         a = dfM.to_numpy()
#         length = len(a)

#         a_shaped = a.reshape(10, int(length/10))
#         a_shaped_transposed = a_shaped.transpose()

#         moist_table=pd.DataFrame(a_shaped_transposed)
#     else:
#         moist_table=find_table(coord, unit, "moist")
#         dim = moist_table['xcoord'].max()
#         moist_table=moist_table.drop(['id','timestamp','number','coordinate','imei','xcoord', 'ycoord'], axis=1)
#         moist_table=moist_table.head(dim*dim)

#     moist_color=['#ffffd9','#41b6c4','#081d58']
#     moist_unit = "%"
#     moist_name="Moisture"

#     mz=[(np.matrix(moist_table[i].tolist())).reshape( int(dim), int(dim)) for i in moist_table]

#     xy_vals=[val*(dim/5) for val in range(0, 6)]
#     z_vals=[val*((len(mz)*dim)/5) for val in range(0, 6)]
#     ticktext = [0,2,4,6,8,10]

#     fig = go.Figure()

#     for i in range(len(moist_table.columns)):
#         r=mz[i]
#         fig.add_trace(go.Surface(z=mz[i]+dim*(i+1), text=mz[i], hoverinfo='text', meta=['Moisture',i], hovertemplate='x:%{x}m <br>y:%{y}m <br>z: %{meta[1]}m <br><b>%{meta[0]}: %{text}</b>'+moist_unit,
#                                     colorscale=moist_color, surfacecolor=mz[i], cmin=10, cmax=20,
#                                     colorbar=dict(x=.95,thickness=10,title='(%)'),opacityscale="max",
#                                     showscale=True, opacity=1, name="{}".format(moist_table.columns[i])))

#     fig.update_yaxes(showgrid=False, scaleanchor='x', showticklabels=False)
#     fig.update_xaxes(showgrid=False, scaleanchor='y',showticklabels=False)

#     fig.update_layout(scene_aspectmode='cube', xaxis =  { 'showgrid': False,'zeroline':False},
#     scene_camera = scene_camera_position,
#     margin={"l": 0, "r": 0, "t": 0, "b": 0},
#     scene = dict(
#                     xaxis = dict(range=[0,int(dim)], title='length (m)', showgrid=False,zeroline=False,showbackground=False,ticktext=ticktext, tickvals=xy_vals),
#                     yaxis = dict(range=[0,int(dim)], title='width (m)',showgrid=False, showbackground=False,ticktext=ticktext, tickvals=xy_vals),
#                     zaxis = dict(showticklabels=True, ticktext=ticktext, tickvals=z_vals, title='height (m)',showgrid=False,zeroline=False, showbackground=False)))

#     return fig


# def get_temp_graph(unit, scene_camera_position):
#     coord='48.972/169.3274'

#     if unit == "2":
#         pathT=os.path.join(BASE_DIR, 'data','temp_v10_6.txt')
#         dim=20 #y=20 z-20

#         dfT = pd.read_csv(pathT, header=None)

#         b = dfT.to_numpy()
#         length = len(b)

#         b_shaped = b.reshape(dim, int(length/dim))
#         b_shaped_transposed = b_shaped.transpose()

#         temp_table=pd.DataFrame(b_shaped_transposed)
#     elif unit == "3":
#         pathT=os.path.join(BASE_DIR, 'data','temp_v11_6.txt')
#         dim=20 #y=20 z-20

#         dfT = pd.read_csv(pathT, header=None)

#         b = dfT.to_numpy()
#         length = len(b)

#         b_shaped = b.reshape(10, int(length/10))
#         b_shaped_transposed = b_shaped.transpose()

#         temp_table=pd.DataFrame(b_shaped_transposed)
#     else:    
#         dim = temp_table['xcoord'].max()
#         temp_table=find_table(coord, unit, "temp")
#         temp_table=temp_table.drop(['id','timestamp','number','coordinate','imei','xcoord', 'ycoord'], axis=1)
#         temp_table=temp_table.head(dim*dim)


    
#     fig=go.Figure()
#     temp_color=['#ffffd9','#5fa1e3','#88bed1','#e85656', '#8c080d']
#     temp_unit = "°C"
#     temp_name="Temperature"

#     tz=[(np.matrix(temp_table[i].tolist())).reshape( int(dim), int(dim)) for i in temp_table]
#     print(9)

#     for i in range(len(temp_table.columns)):
#         r=tz[i]
#         fig.add_trace(go.Surface(z=tz[i]+dim*(i+1), text=tz[i], hoverinfo='text', meta=['Temperature',i], hovertemplate='x:%{x}m <br>y:%{y}m <br>z: %{meta[1]}m <br><b> %{meta[0]}: %{text}</b>'+temp_unit,
#                                  colorscale=temp_color, surfacecolor=tz[i], cmin=0, cmax=20,
#                                  colorbar=dict(x=.95, thickness=10, title='(°C) '),opacityscale="max",
#                                  showscale=True, opacity=1, name="{}".format(temp_table.columns[i])))
    
#     xy_vals=[val*(dim/5) for val in range(0, 6)]
#     z_vals=[val*((len(tz)*dim)/5) for val in range(0, 6)]
#     ticktext = [0,2,4,6,8,10]
    
#     fig.update_yaxes(showgrid=False, scaleanchor='x', showticklabels=False)
#     fig.update_xaxes(showgrid=False, scaleanchor='y',showticklabels=False)

#     fig.update_layout(scene_aspectmode='cube', xaxis =  { 'showgrid': False,'zeroline':False},
#     scene_camera = scene_camera_position,
#     margin={"l": 0, "r": 0, "t": 0, "b": 0},
#     scene = dict(
#                     xaxis = dict(range=[0,int(dim)], title='length (m)', showgrid=False,zeroline=False,showbackground=False,ticktext=[0,2,4,6,8,10], tickvals=xy_vals),
#                     yaxis = dict(range=[0,int(dim)], title='width (m)',showgrid=False, showbackground=False,ticktext=[0,2,4,6,8,10], tickvals=xy_vals),
#                     zaxis = dict(showticklabels=True, title='height (m)',showgrid=False,zeroline=False, showbackground=False,ticktext=[0,2,4,6,8,10], tickvals=z_vals)))

#     return fig
    




# # ------------------------------------------------------------------------------

