
import numpy as np
import pandas as pd
import plotly.graph_objects as go
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
from dash.dash import no_update

#-------------------------------------------------------------------------
#Helper functions to update silo graph 
def change_silo_angle(data, newlySelectedStoreUnit, previouslySelectedStoreUnit, silo, initial_rotation): 
  if newlySelectedStoreUnit is None:
    return show_initial_silo_if_no_unit_selected(silo)

  elif newlySelectedStoreUnit != previouslySelectedStoreUnit:
    return rotate_to_initial_position_if_new_store_selected(newlySelectedStoreUnit, silo, initial_rotation)

  elif data and 'scene.camera' in data:
      return rotate_to_match_figure_if_heatmap_rotated(data, previouslySelectedStoreUnit, silo)

  else:
      return no_update


def show_initial_silo_if_no_unit_selected(silo):
    return None, silo

def rotate_to_initial_position_if_new_store_selected(newlySelectedStoreUnit, silo, initial_rotation):
  silo.update_layout(scene_camera=initial_rotation)
  return newlySelectedStoreUnit, silo

def rotate_to_match_figure_if_heatmap_rotated(data, previouslySelectedStoreUnit, silo):
  silo.update_layout(scene_camera = data['scene.camera'])
  return previouslySelectedStoreUnit, silo

#-------------------------------------------------------------------------
#Shape functions to construct silo 

def silo():
  mainBody = cylinder(radius=2,height=3)
  topCylinder = cylinder(radius=0.75,height=0.25,zOffset=3.6)
  roof = cone(0.75,0.2,0,0,4)
  floor = cone(2.3,0,0,0,0)
  bottomCylinder = cylinder(radius=2.2, height=0.2,zOffset=-0.2)
  top = coneCapped(2,0.5,0,0,4,0.5)

  x1, y1, z1 = circle(2.01, 0)
  x2, y2, z2 = circle(2.01, 1)
  x3, y3, z3 = circle(2.01, 2)
  x4, y4, z4 = circle(2.01, 3)
  circlesRoundMainBody = go.Scatter3d(x = x1.tolist() + x2.tolist() + x3.tolist() + x4.tolist(),
                          y = y1.tolist() + y2.tolist() + y3.tolist() + y4.tolist(),
                          z = z1.tolist() + z2.tolist() + z3.tolist() + z4.tolist(),
                          mode ='lines',
                          line = dict(color='black', width=2),
                          opacity =0.55, showlegend=False)

  ladder = go.Scatter3d(x = [-0.08,-1,-1,-0.08,-0.08,-1,-1,-0.08,-0.08,-1,-1,-0.08,-0.08,-1,-1,-0.08,0.2,0.2,-0.08,-1,-0.7,-0.7,-1],
                          y = [2.5,2.2,2.2,2.5,2.5,2.2,2.2,2.5,2.5,2.2,2.2,2.5,2.5,2.2,2.2,2.5,0.9,0.9,2.5,2.2,0.6,0.6,2.2],
                          z = [0,0,0.5,0.5,1,1,1.5,1.5,2,2,2.5,2.5,3,3,3.5,3.5,3.6,3.8,3.7,3.7,3.8,3.6,3.5],
                          mode ='lines',
                          line = dict(color='black', width=2),
                          opacity =0.55, showlegend=False)


  ladderRungs = go.Scatter3d(x = [-0.08,-0.08,-1,-1],
                          y = [2.5,2.5,2.2,2.2],
                          z = [0,4,4,0],
                          mode ='lines',
                          line = dict(color='black', width=2),
                          opacity =0.55, showlegend=False)

  layout = go.Layout(scene_xaxis_visible=False, scene_yaxis_visible=False, scene_zaxis_visible=False,scene_aspectmode='cube',
                  margin={"l": 0, "r": 0, "t": 0, "b": 300},autosize=True,
                  scene_camera=dict(
                              eye=dict(x=2.5, y=2.5, z=1.25)
                          ))

  return go.Figure(data=[mainBody,top,topCylinder, roof,circlesRoundMainBody,floor, bottomCylinder, ladder, ladderRungs], layout=layout)

def cylinder(radius, height, zOffset=0, startCoord =0, rounded_Amount=40, numberOfJoinUpsHeight=2, colorscale=[[0, 'gray'],[1, 'white']]):
    circumferencePoints = np.linspace(0, 2*np.pi, rounded_Amount)
    heightPoints = np.linspace(startCoord, startCoord+height, numberOfJoinUpsHeight )
    circumferencePoints, heightPoints = np.meshgrid(circumferencePoints, heightPoints)
    x = radius*np.cos(circumferencePoints)
    y = radius*np.sin(circumferencePoints)
    z = heightPoints
    return go.Surface(x=x, y=y, z=heightPoints+zOffset,
                 colorscale = colorscale,
                 showscale=False, opacity=1)

def coneCapped(radius, height, xOffSet, yOffset, zOffset, cappedAtHeight, colorscale=[[0, 'gray'],[1, 'white']]):
  circle = np.linspace(0,2*np.pi,15)
  r = np.linspace(0,radius,50)
  T, R = np.meshgrid(circle, r)

  Y = R * np.cos(T)
  X = R * np.sin(T)
  Z = height*np.sqrt(X**2 + Y**2)

  Z[Z < height*cappedAtHeight] = np.nan

  return go.Surface(x=X+xOffSet, y=Y+yOffset, z=-Z+zOffset,
                  colorscale = colorscale,
                  showscale=False, opacity=1)

def cone(radius, height, xOffSet, yOffset, zOffset, colorscale=[[0, 'gray'],[1, 'white']]):
  numberOfPointsMakingUpCircle = np.linspace(0,2*np.pi,15)
  r = np.linspace(0,radius,50)
  T, R = np.meshgrid(numberOfPointsMakingUpCircle, r)

  Y = R * np.cos(T)
  X = R * np.sin(T)
  Z = height*np.sqrt(X**2 + Y**2)

  return go.Surface(x=X+xOffSet, y=Y+yOffset, z=-Z+zOffset,
                  colorscale = colorscale,
                  showscale=False, opacity=1)


def circle(r, h, numberOfPointsMakingUpCircle=30):
    circlePoints = np.linspace(0, 2*np.pi, numberOfPointsMakingUpCircle)
    x= r*np.cos(circlePoints)
    y = r*np.sin(circlePoints)
    z = h*np.ones(circlePoints.shape)
    return x, y, z

#-------------------------------------------------------------------------
#Helper functions to update temp/moisture graphs 

def get_updated_moisture_graph(unit, scene_camera_position):

    moist_table = get_table('moist', unit)
    dim = get_dim(unit)

    z_values = get_z_values(moist_table, dim)
    fig = plot_heatmap(moist_table, z_values, dim, '%', ['#ffffd9','#41b6c4','#081d58'], 10, 20)
    update_graph_layout(dim, z_values, fig, scene_camera_position)

    return fig


def get_updated_temp_graph(unit, scene_camera_position):
    temp_table = get_table('temp', unit)
    dim = get_dim(unit)
    
    z_values = get_z_values(temp_table, dim)
    fig = plot_heatmap(temp_table, z_values, dim, '°C', ['#ffffd9','#5fa1e3','#88bed1','#e85656', '#8c080d'], 0, 20)
    update_graph_layout(dim, z_values, fig, scene_camera_position)
    
    return fig


def get_table(type, unit):
  if unit == "1"or unit == "3":
    pathM=os.path.join(BASE_DIR, 'data', type + '_v10_6.txt')
    dim=20 #y=20 z-20
    dfM = pd.read_csv(pathM, header=None)

    a = dfM.to_numpy()
    length = len(a)

    a_shaped = a.reshape(20, int(length/20))
    a_shaped_transposed = a_shaped.transpose()

    moist_table=pd.DataFrame(a_shaped_transposed)

  elif unit == "2" or unit == "4":
    pathM=os.path.join(BASE_DIR, 'data',type + '_v11_6.txt')
    dim=10 #y=20 z-20

    dfM = pd.read_csv(pathM, header=None)

    a = dfM.to_numpy()
    length = len(a)

    a_shaped = a.reshape(10, int(length/10))
    a_shaped_transposed = a_shaped.transpose()

    moist_table=pd.DataFrame(a_shaped_transposed)
  return moist_table


def get_dim(unit):
  return 20


def get_z_values(table, dim):
  return [(np.matrix(table[i].tolist())).reshape( int(dim), int(dim)) for i in table]


def plot_heatmap(table, z_values, dim, scale_unit, colorscale, cmin, cmax):
  fig=go.Figure()

  for i in range(len(table.columns)):
      r=z_values[i]
      fig.add_trace(go.Surface(z=z_values[i]+dim*(i+1), text=z_values[i], hoverinfo='text',  meta=['Value',i], hovertemplate='x:%{x}m <br>y:%{y}m <br>z: %{meta[1]}m <br><b>%{meta[0]}: %{text}</b>'+scale_unit,
                                colorscale=colorscale, surfacecolor=z_values[i], cmin=cmin, cmax=cmax,
                                colorbar=dict(x=.95, thickness=10, title=scale_unit,lenmode='fraction', len=0.70),opacityscale="max",
                                showscale=True, opacity=1, name="{}".format(table.columns[i])))
  if scale_unit == "%":
    plt_title= "Moisture"
  elif scale_unit == "°C" :
    plt_title= "Temperature"

  fig.update_layout(
    title={
        'text': plt_title,
        'y':0.9,
        'x':0.5,
        'xanchor': 'center',
        'yanchor': 'top'})

  return fig


def update_graph_layout(dim, z_values, fig, scene_camera_position):
  xy_vals=[val*(dim/5) for val in range(0, dim)]
  z_vals=[val*((len(z_values)*dim)/5) for val in range(0, dim)]
  z_vals=[val*(dim) for val in range(0, dim)]

  ticktext = list(range(0, dim+1))
  tick=[]
  for i in range(0, dim+1,2):
    tick.append(" ")
    tick.append(ticktext[i])
  print(tick)

  fig.update_yaxes(showgrid=False, scaleanchor='x', ticktext= ticktext, showticklabels=True)
  fig.update_xaxes(showgrid=False, scaleanchor='y',ticktext= ticktext, showticklabels=True)

  fig.update_layout(scene_aspectmode='cube', xaxis =  { 'showgrid': False,'zeroline':False},
  scene_camera = dict(
    eye=dict(x=2.8, y=2.8, z=1.5)
),
  margin={"l": 0, "r": 0, "t": 0, "b": 0},
  scene = dict(
                  xaxis = dict(range=[0,int(dim)], title='length (m)', showgrid=False,zeroline=False,showbackground=False,ticktext=ticktext, tickvals= ticktext,dtick = 2,tickmode='linear',tick0=1),
                  yaxis = dict(range=[0,int(dim)], title='width (m)',showgrid=False, showbackground=False,ticktext=ticktext, tickvals= ticktext,dtick = 2, tickmode='linear',tick0=1),
                  zaxis = dict( title='height (m)',showgrid=False,zeroline=False, showbackground=False,ticktext=tick, tickvals=z_vals,)))
