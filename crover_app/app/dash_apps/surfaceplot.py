import pandas as pd
import plotly.express as px  # (version 4.7.0)
import plotly.graph_objects as go
import numpy as np
import os
import dash  # (version 1.12.0) pip install dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import base64
from django_plotly_dash import DjangoDash
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
app = DjangoDash('surfaceplot')

# ------------------------------------------------------------------------------
# Import and clean data (importing csv into pandas)

moist_table = pd.DataFrame()
for i in range(20):
    data_path = os.path.join(BASE_DIR, 'maps', 'moist_map_array_'+str(i)+'.txt')
    df = pd.read_csv(data_path, delimiter = ',', header=None)
    moist_table[i] = df.values.flatten()


# ------------------------------------------------------------------------------
# App layout
app.layout = html.Div([

    #html.H1("Moisture dashboard", style={'text-align': 'center'}),
  
    html.Div(id='output_container', children=[]), 

    dcc.Graph(id='surface_plot', figure={}),
    
    dcc.Checklist(id="slct_array",
        options=[
            {"label": "array0", "value": 0},
            {"label": "array1", "value": 1},
            {"label": "array2", "value": 2},
            {"label": "array3", "value": 3},
            {"label": "array4", "value": 4},
            {"label": "array5", "value": 5},
            {"label": "array6", "value": 6},
            {"label": "array7", "value": 7},
            {"label": "array8", "value": 8},
            {"label": "array9", "value": 9},
            {"label": "array10", "value": 10},
            {"label": "array11", "value": 11},
            {"label": "array12", "value": 12},
            {"label": "array13", "value": 13},
            {"label": "array14", "value": 14},
            {"label": "array15", "value": 15},
            {"label": "array16", "value": 16},
            {"label": "array17", "value": 17},
            {"label": "array18", "value": 18},
            {"label": "array19", "value": 19}
        ],
        value=[0,1,2,3,4,5,6,7,8],
        style={'padding-left':'0px','padding-right':'0px'},
        labelStyle={'display': 'inline-block'},
    ),  
    
    html.Br(),


])


# ------------------------------------------------------------------------------
# Connect the Plotly graphs with Dash Components
@app.callback(
    [Output(component_id='output_container', component_property='children'), #output 1 - year
     Output(component_id='surface_plot', component_property='figure')], #output 2 - figure
    [Input(component_id='slct_array', component_property='value')] #slider
)

def update_graph(option_slctd):
    print(option_slctd)
    container = "Layered Heatmap{}".format(option_slctd)
    z=[(np.matrix(moist_table[i].tolist())).reshape(40,40) for i in range(20)]
    data=[]

    data.append(go.Surface(z=(np.matrix(moist_table.mean(axis=1).tolist())).reshape(40,40), showscale=True, opacity=0.9))

    for i in range(len(option_slctd)):
       
        data.append(go.Surface(z=z[i]+3*i, showscale=False, opacity=0.9, name="array {}".format(option_slctd[i])))
   
    fig = go.Figure(data=data)
    
    image_filename = os.getcwd() + '/static/images/silo.png' 
    # image_filename ="{% STATICFILES_DIRS '/images/silo.png/' %}" # replace with your own image
    encoded_image = base64.b64encode(open(image_filename, 'rb').read())
    # fig.update_yaxes(showgrid=False, scaleanchor='x', range=(3000, 0))
    fig.add_layout_image(
        dict(
            source=('data:image/png;base64,{}'.format(encoded_image.decode())),
            xref="x",
            yref="y",
            x=-5.5,
            y=7.7,
            sizex=15.5,
            sizey=11.5,
            sizing="stretch",
            opacity=0.3,
            layer="above")
    )
    fig.update_yaxes(showgrid=False, scaleanchor='x', showticklabels=False)
    fig.update_xaxes(showgrid=False, scaleanchor='y',showticklabels=False)
    
    
    fig.update_layout( autosize=False,
                  width=750, height=700,
                  margin=dict(l=50, r=50, b=20, t=90),
                  scene = dict(
                    zaxis_title='Layers/Array', zaxis = dict(showticklabels=False)
                    ),)

    return container, fig #output1-year #output2-figure


# ------------------------------------------------------------------------------
