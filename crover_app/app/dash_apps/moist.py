import pandas as pd
import plotly.express as px  # (version 4.7.0)
import plotly.graph_objects as go
import numpy as np
import os
import dash  # (version 1.12.0) pip install dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from django_plotly_dash import DjangoDash

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

app = DjangoDash('moist')

# ------------------------------------------------------------------------------
# Import and clean data (importing csv into pandas)

moist_table = pd.DataFrame()
for i in range(20):
    data_path = os.path.join(BASE_DIR, 'maps', 'moist_map_array_'+str(i)+'.txt')
    df = pd.read_csv(data_path, delimiter = ',', header=None)
    moist_table[i] = df.values.flatten()
# ------------------------------------------------------------------------------
# App layout
app.layout = html.Div([

    html.H1("Moisture dashboard", style={'text-align': 'center'}),

    dcc.Dropdown(id="slct_array",
                 options=[
                     {"label": "mean", "value": "mean"},
                     {"label": "array0", "value": 0},
                     {"label": "array1", "value": 1},
                     {"label": "array2", "value": 2},
                     {"label": "array3", "value": 3},
                     {"label": "array4", "value": 4},
                     {"label": "array5", "value": 5},
                     {"label": "array6", "value": 6},
                     {"label": "array7", "value": 7},
                     {"label": "array8", "value": 8},
                     {"label": "array9", "value": 9},
                     {"label": "array10", "value": 10},
                     {"label": "array11", "value": 11},
                     {"label": "array12", "value": 12},
                     {"label": "array13", "value": 13},
                     {"label": "array14", "value": 14},
                     {"label": "array15", "value": 15},
                     {"label": "array16", "value": 16},
                     {"label": "array17", "value": 17},
                     {"label": "array18", "value": 18},
                     {"label": "array19", "value": 19}
                      ],
                 multi=False,
                 value="mean",
                 style={'width': "40%"}
                 ),

    html.Div(id='output_container', children=[]),
    html.Br(),

    dcc.Graph(id='moist_map', figure={})

])


# ------------------------------------------------------------------------------
# Connect the Plotly graphs with Dash Components
@app.callback(
    [Output(component_id='output_container', component_property='children'), #output 1 - year
     Output(component_id='moist_map', component_property='figure')], #output 2 - figure
    [Input(component_id='slct_array', component_property='value')]
)
def update_graph(option_slctd):

    print(option_slctd)
    print(type(option_slctd))
    container = "The array chosen by user was: {}".format(option_slctd)

    if option_slctd=="mean":
        a=moist_table.mean(axis=1)
    else:
        a=moist_table[option_slctd]

    b=np.matrix(a.tolist())
    c=b.reshape(40,40)


    #Plotly Graph Objects (GO)
    fig = go.Heatmap(
        x= np.arange(40),
        y= np.arange(40),
        z= c,
        type='heatmap',
        colorscale='Viridis'    
    )

    data = [fig]
    fig = go.Figure(data=data)
    #fig.show()



    
    # fig.update_layout(
    #     title_text="Bees Affected by Mites in the USA",
    #     title_xanchor="center",
    #     title_font=dict(size=24),
    #     title_x=0.5,
    #     geo=dict(scope='usa'),
    # )

    return container, fig #output1-year #output2-figure


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    app.run_server(debug=True)