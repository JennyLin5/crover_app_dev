from django.shortcuts import render
from django.http import HttpResponse
from plotly.offline import plot
from plotly.graph_objs import Scatter
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.contrib.auth import logout, authenticate, login
from django.contrib import messages
from django.shortcuts import redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group

import plotly.graph_objects as go
import copy, json, datetime #, requests
import csv, io, os
import pandas as pd
import numpy as np

from .models import Moist
from .forms import CreateUserForm
from .decorators import unauthenticated_user, allowed_users, manager_only
from .helper import get_all_users, get_user, find_table, get_sites_names,get_sites_coord,get_total_site_capacity,get_unit_capacities

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
STATICFILES_DIRS = os.path.join(BASE_DIR, 'static') # new

@unauthenticated_user
def registerPage_view(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            # Add user in customer group as default login
            user = form.save()
            username = form.cleaned_data.get('username')
            groupname = form.cleaned_data.get('groupname')
            group = Group.objects.get(name=groupname)
            user.groups.add(group)

            messages.success(request, 'Account was created for ' + username)
            return redirect('login')
    context = {'form':form}
    return render(request, "register.html", context)

@unauthenticated_user
def loginPage_view(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        
        if user is not None:
            login(request, user)
            return redirect('app')
        else:
            messages.info(request, 'Username OR password is incorrect')

    context={}
    return render(request, "login.html", context)

def logout_request(request):
    logout(request)
    return redirect("login")

@login_required(login_url='login')
def no_access_view(request, *args, **kwargs):
    print(args, kwargs)
    print(request.user)
    context={"username": request.user}
    return render(request, "no_access.html", context)

@login_required(login_url='login')
def app_view(request, *args, **kwargs):
    print(args, kwargs)
    print(request.user)
    context={"num_sites": ["A",2,"Jenny",4,5]}
    return render(request, "app.html", context)

@login_required(login_url='login')
@manager_only
def graphs_view(request, *args, **kwargs):
    # x_data = [0,1,2,3]
    # y_data = [x**2 for x in x_data]
    # plot_div = plot([Scatter(x=x_data, y=y_data,
    #                     mode='lines', name='test',
    #                     opacity=0.8, marker_color='green')],
    #            output_type='div')

    moisture_plot=plot_moisture()
    return render(request, "graphs.html", context={'moisture_plot': moisture_plot })

@login_required(login_url='login')
@allowed_users(['Staff', 'Managers','Customers'])
def storage_view(request, *args, **kwargs):
    print(args, kwargs)
    print(request.user)
    moisture_plot=plot_moisture()

    context = {"id" : 5,
                "location" : get_sites_coord(),
                "site_numbers" : get_sites_names(),
                "manager" : "Crover Ltd",
                'moisture_plot': moisture_plot}
    return render(request, "storage.html", context)

@login_required(login_url='login')
@allowed_users(['Staff', 'Managers'])
def site_view(request, *args, **kwargs):
    print(args, kwargs)
    # print(request.user)

    # p =  get_sites_coord()
    # q=p.strip("(),'''")
    # x=q.replace('''''', '')
    

    # context = {"id" : 5,
    #             "location" : x.replace('/', ', '),
    #             "total_capacity" : get_total_site_capacity(),
    #             "unit_capacities":get_unit_capacities(),
    #             "site_numbers" : get_sites_names(),
    #             "manager" : "Crover Ltd",
    #             }

    context = {"id" : 5,
                "location" : "Edinburgh",
                "total_capacity" : get_total_site_capacity(),
                "unit_capacities":get_unit_capacities(),
                "site_numbers" : get_sites_names(),
                "manager" : "Crover Ltd",
                }
    return render(request, "site.html", context=context)

@login_required(login_url='login')
@allowed_users(['Managers'])
def user_view(request, *args, **kwargs):
    print(args, kwargs)
    print(request.user)
    context={"get_all_users":get_all_users()}
    return render(request, "user.html", context)

@login_required(login_url='login')
def profile_view(request, *args, **kwargs):
    context={"get_user":get_user(request.user)}
    return render(request, "profile.html", context)

@login_required(login_url='login')
def charts_view(request, *args, **kwargs):
    # get from db
    x = Moist.objects.values_list('id','array0','array1')
    print(x)
    print(args, kwargs)
    print(request.user)
    context = {"data" : x}
    return render(request, "charts.html", context)

# def test_view(request, data=None, **kwargs):
#     print("test")
#     print(data)
#     context = {"data" : data}
#     return render(request, "test.html", context)

@login_required(login_url='login')
def test_view(request, *args, **kwargs):
    #data = request.session['data']
    # request session requires db
    print("test")
    #processed = process_data(data)
    context = {
                "sites_names" : get_sites_names(),
                }
    # print(context.get(moist_data))
    #print(processed[0].get('array0')) layer 1 first element

    # # write to db
    # moist_instance = Moist.objects.create(array0=processed[0].get('array0'))
    # moist_instance.save(update_fields=['array0'])
    return render(request, "test.html", context)

def process_data(data):
    print(data)
    #print(data.values())
    lst = list(data.values())[0]
    print("list1")
    print(lst[0])
    return lst
    

@csrf_exempt # exempt verifying CSRF token
#@require_POST # to ensure the request is only POST.
def webhook_view(request,*args, **kwargs):
    print(request.method)

    # data = json.loads(request.body)
    # #meta = copy.copy(request.META)
    
    # save_data_to_file(data)

    # request.session['data'] = data # type:dict

    return HttpResponse(200)

@allowed_users(['Staff', 'Managers','Customers'])
def contact_view(request, *args, **kwargs):
    context = {"id" : 5,
                "location" : "Glasgow",
                "manager" : "Crover Ltd"}
    return render(request, "contact.html", context)

def save_data_to_file(data):
   with open('data.txt', 'w') as outfile:
        json.dump(data, outfile)

def plot_moisture():
    moist_table = pd.DataFrame()
    for i in range(20):
        data_path = os.path.join(BASE_DIR, 'static/data/maps/moist_map_array_'+str(i)+'.txt')
        print(data_path)
        df = pd.read_csv(data_path, delimiter = ',', header=None)
        moist_table[i] = df.values.flatten()

    a=moist_table.mean(axis=1)
    b=np.matrix(a.tolist())
    c=b.reshape(40,40)
    fig = go.Heatmap(
        x= np.arange(40),
        y= np.arange(40),
        z= c,
        type='heatmap',
        colorscale='Viridis'    
    )

    data = [fig]
    fig = go.Figure(data=data)
    #fig.show()

    moisture_plot = plot(fig, output_type='div')
    return moisture_plot