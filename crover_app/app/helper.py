from django.db import connection
from django.db import connections
import pymysql
from django.contrib.auth.models import User
from matplotlib import pyplot as plt
import csv
import json
import os
import pandas as pd
import numpy as np
import logging

def get_all_users():
    # userList =User.objects.values()
    userList =User.objects.all()
    return userList

def get_user(u):
    return User.objects.filter(username=u)

def connect_db():
    conn = pymysql.connect(
        host='crover-aws-restored.c1zyq2yimk50.eu-west-2.rds.amazonaws.com',
        port=int(3306),
        user="admin",
        passwd="password",
        db="crover_aws_test")
    return conn.cursor()
    
def get_sites_names():
    # cursor = connect_db()
    # l = ['moistS','moistM','moistL','tempS','tempM','tempL']
    # names=[]
    # for i in l:
    #     sql = "SELECT DISTINCT number FROM {}".format(i)
    #     print(sql)
    #     cursor.execute(sql)
    #     sites=cursor.fetchall()
    #     for j in sites:
    #         names.append((j)) # formatting purpose
    # new=[]
    # z=list(set(names))
    # for i in range(len(z)):
    #     new.append(z[i][0])
    new=[1,2,3]
    return new # list of names '[1,2,3]'

def get_sites_coord():
    cursor = connect_db()
    l = ['moistS','moistM','moistL','tempS','tempM','tempL']
    names=[]
    for i in l:
        sql = "SELECT coordinate FROM {} limit 1".format(i)
        print(sql)
        cursor.execute(sql)
        sites=cursor.fetchall()
        names = ''.join(map(str, sites)) # formatting purpose
    return names #"('48.972/169.3274',)"

#moist table should be == temp table
def get_site_units(coord):
    cursor = connect_db()
    cursor.execute('SELECT DISTINCT number FROM moistL WHERE coordinate = %s', moist)
    sites=cursor.fetchall()
    return sites


# =======================
# Get Moist Data

def get_moistS_data(unit, coord): # unit = 1 coord = '48.972/169.3274'
    cursor = connect_db()
    cursor.execute("SELECT * FROM moistS WHERE Number=%s AND coordinate=%s;", (unit, coord))
    colnames = cursor.description
    headers =[]
    for row in colnames:
        headers.append(row[0])
    table=pd.DataFrame(cursor.fetchall())
    table.columns=headers
    return table

def get_moistM_data(unit, coord): # unit = 1 coord = '48.972/169.3274'
    cursor = connect_db()
    cursor.execute("SELECT * FROM moistM WHERE Number=%s AND coordinate=%s;", (unit, coord))
    colnames = cursor.description
    headers =[]
    for row in colnames:
        headers.append(row[0])
    table=pd.DataFrame(cursor.fetchall())
    table.columns=headers
    return table

def get_moistL_data(unit, coord): # unit = 1 coord = '48.972/169.3274'
    cursor = connect_db()
    cursor.execute("SELECT * FROM moistL WHERE Number=%s AND coordinate=%s;", (unit, coord))
    colnames = cursor.description
    headers =[]
    for row in colnames:
        headers.append(row[0])
    table=pd.DataFrame(cursor.fetchall())
    table.columns=headers
    return table

# =======================
# Get TEMP data
def get_tempS_data(unit, coord): # unit = 1 coord = '48.972/169.3274'
    cursor = connect_db()
    cursor.execute("SELECT * FROM tempS WHERE Number=%s AND coordinate=%s;", (unit, coord))
    colnames = cursor.description
    headers =[]
    for row in colnames:
        headers.append(row[0])
    table=pd.DataFrame(cursor.fetchall())
    table.columns=headers
    return table

def get_tempM_data(unit, coord): # unit = 1 coord = '48.972/169.3274'
    cursor = connect_db()
    cursor.execute("SELECT * FROM tempM WHERE Number=%s AND coordinate=%s;", (unit, coord))
    colnames = cursor.description
    headers =[]
    for row in colnames:
        headers.append(row[0])
    table=pd.DataFrame(cursor.fetchall())
    table.columns=headers
    return table

def get_tempL_data(unit, coord): # unit = 1 coord = '48.972/169.3274'
    cursor = connect_db()
    cursor.execute("SELECT * FROM tempL WHERE Number=%s AND coordinate=%s;", (unit, coord))
    colnames = cursor.description
    headers =[]
    for row in colnames:
        headers.append(row[0])
    table=pd.DataFrame(cursor.fetchall())
    table.columns=headers
    return table

# =======================
# For MOIST get all units in each of the different sized storage units (S,M,L)
def get_moistS(cursor): # unit = 1 coord = '48.972/169.3274'
    cursor.execute('SELECT DISTINCT number FROM moistS')
    moist=cursor.fetchall()
    return moist

def get_moistM(cursor): # unit = 1 coord = '48.972/169.3274'
    cursor.execute('SELECT DISTINCT number FROM moistM')
    moist=cursor.fetchall()
    return moist

def get_moistL(cursor): # unit = 1 coord = '48.972/169.3274'
    cursor.execute('SELECT DISTINCT number FROM moistL')
    moist=cursor.fetchall()
    return moist

# =======================
# For TEMP get all units in each of the different sized storage units (S,M,L)
def get_tempS(cursor): # unit = 1 coord = '48.972/169.3274'
    cursor.execute('SELECT DISTINCT number FROM tempS')
    temp=cursor.fetchall()
    return temp

def get_tempM(): # unit = 1 coord = '48.972/169.3274'
    cursor = connect_db()
    cursor.execute('SELECT DISTINCT number FROM tempM')
    temp=cursor.fetchall()
    return temp

def get_tempL(): # unit = 1 coord = '48.972/169.3274'
    cursor = connect_db()
    cursor.execute('SELECT DISTINCT number FROM tempL')
    temp=cursor.fetchall()
    return temp

# Convert to date
def convert_to_date(table):
    date = []
    for i in table:
        date.append(i.date())
    return date

# Convert to time
def convert_to_time(table):
#     from datetime import datetime
    time = []
    for i in table:
        time.append(i)
#         print(datetime.datetime.strptime(str(i), "%Y-%m-%dT%H:%M:%S.%f%z"))#T14:14:32.000000000
#         datetime.strptime(str(i), "%d %B, %Y")
    return time
        
# def mean_time(unit, coord):
#     x=pd.DataFrame()
#     table=pd.DataFrame(get_moist_time_data(unit,coord))
#     x['timestamp'] = convert_to_date(table[0])

#     pd.DataFrame(get_moist_data(unit,coord))
#     array_mean=moist_data.mean(axis=1)
#     x['mean']=array_mean
#     return x

# def plot_line(res):
#     from matplotlib import pyplot
#     res.plot()
#     return pyplot.show()

    # Get the mean of unit per timestamp
def unit_means(unit, coord, table):
    if table == "moist" and size == "S":
        table = get_moistS_data(unit,coord)
    elif table == "moist" and size == "M":
        table = get_moistM_data(unit,coord)
    elif table == "moist" and size == "L":
        table = get_moistL_data(unit,coord)
    elif table == "temp" and size == "S":
        table = get_tempS_data(unit,coord)
    elif table == "temp" and size == "M":
        table = get_tempM_data(unit,coord)
    else:
#         table == "temp" and size == "L"
        table = get_tempL_data(unit,coord)
        
    x=pd.DataFrame()
    #for each timestamp
    df = [x for _, x in table.groupby(table['timestamp'])]
    x['timestamp'] = convert_to_time(set(table['timestamp']))
    for i in df: 
        table=i.drop(['id','timestamp','number','coordinate','imei','xcoord', 'ycoord'], axis=1)
        array_mean=table.mean(axis=1) # column mean
        m=array_mean.mean(axis=0) # row mean
        x['mean']=m # mean of storage unit at timeframe
    return x

def mean_time_threshold(threshold,table, size): 
    cursor = connect_db()
    if table == "moist" and size == "S":
        data = get_moistS(cursor)
#         table = moist_unit_means(u, coord)
    elif table == "moist" and size == "M":
        data = get_moistM(cursor)
    elif table == "moist" and size == "L":
        data = get_moistL(cursor)
    elif table == "temp" and size == "S":
        data = get_tempS(cursor)
    elif table == "temp" and size == "M":
        data = get_tempS(cursor)
    else:
    #table == "temp" and size == "L"
        data = get_tempS(cursor)

    result=pd.DataFrame()
    for u in data:
        x = unit_means(u, coord, table)
        if float(x['mean']) < threshold:
            result=result.append(x)
    print(result)
    # Plot
    if result.empty:
        return plt.plot()
    else:
        return result.plot(x='timestamp',y='mean',marker='o', c='red')
    
def get_table_data(coord, unit, table_name):
    cursor = connect_db()
    try:
        sql = "SELECT * FROM {table_name} WHERE coordinate='{coord}' AND Number={unit}".format(table_name=table_name, coord=coord, unit=unit)
        print(sql)
        cursor.execute(sql)
        colnames = cursor.description
        headers =[]
        for row in colnames:
            headers.append(row[0])
        table=pd.DataFrame(cursor.fetchall())
        table.columns=headers
    except:
        table=pd.DataFrame({'A' : []})
    return table

# find coord and unit in each table param (coord, unit, moist/temp)
def find_table(coord, unit, table_type):
    table = pd.DataFrame()
    count=0
    while table.empty == True and count < 1:
        if table_type == "moist":
            table = get_table_data( coord,unit, "moistL")
            if table.empty == True:
                table = get_table_data( coord,unit, "moistM")
            if table.empty == True: 
                table = get_table_data( coord,unit, "moistS")
        elif table_type == "temp":
            table = get_table_data( coord, unit,"tempS")
            if table.empty == True:
                table = get_table_data( coord,unit, "tempL")
            if table.empty == True:
                table = get_table_data( coord, unit,"tempM")
        else:
            table=[]
            print("table incorrect")
            break
        count=count+1
    return table

# Get the mean of unit per timestamp
def unit_table_mean(coord,unit, table):
    x=pd.DataFrame()
    #for each timestamp
    df = [x for _, x in table.groupby(table['timestamp'])]
    x['timestamp'] = convert_to_time(set(table['timestamp']))
    for i in df: 
        table=i.drop(['id','timestamp','number','coordinate','imei','xcoord', 'ycoord'], axis=1)
        array_mean=table.mean(axis=1) # column mean
        m=array_mean.mean(axis=0) # row mean
        x['mean']=m # mean of storage unit at timeframe
    return x


#mean of all storage unit for table 
def mean_time_threshold(threshold,coord,unit, table_type): 
   
    try: 
        data=find_table(coord, unit, table_type)
    except:
        data=[]
        print("table is not correct")

    result=pd.DataFrame()
    for u in data:
        x = unit_table_mean( coord, u[0],data)
        if float(x['mean']) < threshold:
            result=result.append(x)
#     print(result)
    # Plot
    if result.empty:
        return plt.plot()
    else:
        return result.plot(x='timestamp',y='mean',marker='o', c='red')



def get_unit_capacities():
    # capacity_list=[]
    # coord='48.972/169.3274'
    # cursor = connect_db()
    # # how many units per table
    # tables_list=['tempS'] # deleted tempL
    # for i in tables_list:
    #     #get distinct unit for each table
    #     sql1 = "SELECT DISTINCT number FROM {i} WHERE coordinate='{coord}'".format(i=i, coord=coord)
    #     cursor.execute(sql1)
    #     u = list(cursor.fetchall())

    #     for j in range(len(u)):
    #         unit=str(u[j]).strip("(),'''")
    #         sql = "SELECT * FROM {i} WHERE coordinate='{coord}' AND Number = {unit}".format(i=i, coord=coord, unit=unit)
    #         cursor.execute(sql)

    #         colnames = cursor.description
    #         print("col names col names col names col names")
    #         print(colnames)
    #         headers =[]
    #         for row in colnames:                
    #             headers.append(row[0])
    #         print("HEADER HEADER HEADER")
    #         print(headers)
            
    #         d=pd.DataFrame(cursor.fetchall())
    #         d.columns=headers
    #         max_val = d['xcoord'].max()
    #         unit_no = str(u[j]).strip("(),'''")
    #         val=max_val*max_val*(max_val/2)*0.79 #tonnes
    #         capacity_list.append([unit_no,max_val,val])

    capacity_list = [[1, 'Winter Wheat', 6, 12, 960, 'success', 'smile'],
    [2,' Malting Barley', 6, 12, 1240, 'success', 'smile'],
    [3, 'Winter Wheat', 8, 13, 780, 'danger', 'frown'],
    [4, 'Malting Barley', 4, 11, 1240, 'danger', 'frown']
    ]

    return capacity_list #[[(1,), 20, 3160.0],...

def get_total_site_capacity():
    df= get_unit_capacities()
    sum=0
    for i in df:
        sum=sum+i[2]
    return sum #tonnes