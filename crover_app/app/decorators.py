from django.http import HttpResponse
from django.shortcuts import redirect

# Check if user is authenticated
def unauthenticated_user(view_func):
    def wrapper_func(request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('app')
        else:
            return view_func(request, *args, **kwargs)
    return wrapper_func

# Checks if user is allowed to access page depending on groups passes through allowed_roles
def allowed_users(allowed_roles=[]):
    def decorator(view_func):
        def wrapper_func(request, *args, **kwargs):
            group = None
            if request.user.groups.exists():
                group = request.user.groups.all()[0].name
            print(group)
            if group in allowed_roles:
                return view_func(request, *args, **kwargs)
            else:
                #return HttpResponse('You are not authorised to view this')
                return redirect('no_access')
            return view_func(request, *args, **kwargs)
        return wrapper_func
    return decorator

# For main page 'app' only manager allows
def manager_only(view_func):
    def wrapper_function(request, *args, **kwargs):
        group = None
        if request.user.groups.exists():
            group = request.user.groups.all()[0].name
        # if group == 'Clients' or group == 'Staff':
        #     return redirect('login')
        if group == 'Managers':
            return view_func(request, *args, **kwargs)
        else:
             return redirect('login')
    return wrapper_function