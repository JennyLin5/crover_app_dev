# from django.forms import ModelForms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

GROUP_CHOICES=[
    ('Clients', 'Clients'),
    ('Staff','Staff'),
    ('Managers','Managers'),
]
class CreateUserForm(UserCreationForm):
    username = forms.CharField()
    email = forms.CharField()
    password1 = forms.CharField(max_length=32, widget=forms.PasswordInput)
    password2 = forms.CharField(max_length=32, widget=forms.PasswordInput)
    groupname = forms.CharField(widget=forms.Select(choices=GROUP_CHOICES, attrs={'class':'form-control'}))
    class Meta:
        model = User
        fields = ['username', 'email', 'password1','password2','groupname',]